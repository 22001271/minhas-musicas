//
//  MyCell.swift
//  Minhas Músicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class MyCell: UITableViewCell {
    
    
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var artista: UILabel!
    @IBOutlet weak var capa: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
