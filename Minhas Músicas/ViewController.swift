//
//  ViewController.swift
//  Minhas Músicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct musica {
    let nomeMusica: String
    let nomeAlbum: String
    let nomeCantor: String
    let nomeImagemPequena: String
    let nomeImagemGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaMusica: [musica] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let musica = self.listaMusica[indexPath.row]
        
        cell.nome.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.artista.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.nomeImagemPequena)
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirInfo", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let infoMusicaViewController = segue.destination as! InfoMusicaViewController
        let indice = sender as! Int
        let musica = self.listaMusica[indice]
        
        infoMusicaViewController.nomeImagem = musica.nomeImagemGrande
        infoMusicaViewController.nomeMusica = musica.nomeMusica
        infoMusicaViewController.nomeAlbum = musica.nomeAlbum
        infoMusicaViewController.nomeCantor = musica.nomeCantor
        
        
    }
    
    
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        tableview.dataSource = self
        
        self.listaMusica.append(musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valenca", nomeImagemPequena: "capa_alceu_pequeno", nomeImagemGrande: "capa_alceu_grande"))
        self.listaMusica.append(musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrande: "capa_zeca_grande"))
        self.listaMusica.append(musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImagemPequena: "capa_adoniran_pequeno", nomeImagemGrande: "capa_adhoniran_grande"))
}


}

